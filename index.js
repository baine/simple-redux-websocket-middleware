
import WebSocket from 'isomorphic-ws';

let types = {
  request: { // these must be dispatched from the application
    OPEN:  'WEBSOCKET/REQUEST/OPEN',
    CLOSE: 'WEBSOCKET/REQUEST/CLOSE',
    SEND:  'WEBSOCKET/REQUEST/SEND'
  },
  lifecycle: { // these originate from the middleware
    OPENING:      'WEBSOCKET/LIFECYCLE/OPENING',
    CLOSING:      'WEBSOCKET/LIFECYCLE/CLOSING',
  },
  error: { // these originate from the middleware
    OPEN_FAILED:  'WEBSOCKET/ERROR/OPEN_FAILED',
    CLOSE_FAILED: 'WEBSOCKET/ERROR/CLOSE_FAILED',
    SEND_FAILED:  'WEBSOCKET/ERROR/SEND_FAILED',
  },
  event: { // these originate from the websocket client
    CLOSE:   'WEBSOCKET/EVENT/CLOSE',
    ERROR:   'WEBSOCKET/EVENT/ERROR',
    OPEN:    'WEBSOCKET/EVENT/OPEN',
    MESSAGE: 'WEBSOCKET/EVENT/MESSAGE'
  }
};

export default () => (makeHandler({}));

function makeHandler(sockets) {
  return { dispatch } => next => function(action) {
    switch(action.type) {
    case types.request.OPEN:
      openSocket(sockets, dispatch, action.payload);
      break;
    case types.request.CLOSE:
      closeSocket(sockets, dispatch, action.payload);
      break;
    case types.request.SEND:
      sendMessage(sockets, dispatch, action.payload);
      break;
    default:
    }
    return next(action);
  };
}

function sendMessage(sockets, dispatch, { id, message }) {
  let socket = sockets[id];
  if (!socket) {
    dispatch({
      type: types.error.SEND_FAILED,
      id,
      reason: noSuchWebSocket(id)
    });
  } else if (socket.readyState !== WebSocket.OPEN) {
    dispatch({
      type: types.error.SEND_FAILED,
      id,
      reason: webSocketInState(id, socket)
    });
  } else {
    socket.send(message);
  }
}

function openSocket(sockets, dispatch, { id, url }) {
  let socket = sockets[id];
  if (socket && (socket.readyState == WebSocket.CONNECTING || socket.readyState == WebSocket.OPEN)) {
    dispatch({
      type: types.error.OPEN_FAILED,
      payload: {
        id,
        reason: webSocketInState(id, socket)
      }
    });
  } else {
    socket = new WebSocket(url);
    sockets[id] = socket;

    socket.onclose = ({ type, code, reason, wasClean }) => dispatch({
      type: types.event.CLOSE,
      payload: {
        id,
        event: { type, code, reason, wasClean }
      }
    });
    socket.onerror = ({ type, message }) => dispatch({
      type: types.event.ERROR,
      payload: {
        id,
        event: { type, message }
      }
    });
    socket.onopen = ({ type }) => dispatch({
      type: types.event.OPEN,
      payload: {
        id,
        event: { type }
      }
    });
    socket.onmessage = ({ data }) => dispatch({
      type: types.event.MESSAGE,
      payload: {
        id,
        event: { type, data }
      }
    });
  }
}

function closeSocket(sockets, dispatch, { id }) {
  let socket = sockets[id];
  if (socket) {
    switch(socket.readyState) {
    case WebSocket.CONNECTING:
    case WebSocket.OPEN:
      socket.close();
      dispatch({
        type: types.lifecycle.CLOSING,
        id
      });
      break;
    case WebSocket.CLOSING:
    case WebSocket.CLOSED:
      dispatch({
        type: types.error.CLOSE_FAILED,
        reason: webSocketInState(id, socket),
        id
      });
      break;
    }
  } else {
    dispatch({
      type: types.error.CLOSE_FAILED,
      reason: noSuchWebSocket(id),
      id
    });
  }
}

let statesMap = (function(keys) {
  let result = {};
  for (let k of keys) {
    result[WebSocket[k]] = k;
  }
  return result;
})(['CONNECTING', 'OPEN', 'CLOSING', 'CLOSED']);

let webSocketInState = (id, socket) =>
    `websocket ${id} is in state ${statesMap[socket.readyState]}`;
let noSuchWebSocket = (id) => `no such websocket ${id} known`;
